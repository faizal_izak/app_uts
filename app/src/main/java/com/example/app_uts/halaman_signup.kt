package com.example.app_uts

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btnSignUp
import kotlinx.android.synthetic.main.activity_login.edtxregEmail
import kotlinx.android.synthetic.main.activity_login.edtxregPassword
import kotlinx.android.synthetic.main.activity_signup.*

class halaman_signup:AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnSignUp.setOnClickListener(this)
        txKembali.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.txKembali -> {
                var intent = Intent(this, halaman_login::class.java)
                startActivity(intent)
            }
        }
        var email = edtxregEmail.text.toString()
        var password = edtxregPassword.text.toString()

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Username / password Harap Di Isi", Toast.LENGTH_LONG).show()
        }
        else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password).addOnCompleteListener{
                progressDialog.hide()
                if(!it.isSuccessful) return@addOnCompleteListener
                Toast.makeText(this,"Berhasil Registrasi", Toast.LENGTH_SHORT).show()
            }
                .addOnFailureListener{
                    progressDialog.hide()
                    Toast.makeText(this,"Username/Password Tidak Valid", Toast.LENGTH_SHORT).show()
                }
        }



    }
}