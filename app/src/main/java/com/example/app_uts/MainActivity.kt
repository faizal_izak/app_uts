package com.example.app_uts

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var iv: ImageView

    lateinit var pref : SharedPreferences
    var setwar =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iv = findViewById(R.id.iv)
        val set1: Animation = AnimationUtils.loadAnimation(this, R.anim.rcanim)
        val set: Animation = AnimationUtils.loadAnimation(this, R.anim.mytransition)
        iv.startAnimation(set)

        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        setwar = pref.getString("warna","").toString()


        val intent =  Intent(this, halaman_login::class.java)
        /*  val handler = Handler()
          handler.postDelayed({

              startActivity(intent)
          }, 5000)
         */

        val tim: Thread
        tim = Thread {
            try {
                Thread.sleep(4000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } finally {
                startActivity(intent)
                finish()
            }
        }
        tim.start()
//        warna()
    }
}