package com.example.app_uts

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_wisata.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class halaman_wisata: AppCompatActivity(), View.OnClickListener {
    val COLLECTION = "wisata"
    val F_ID = "id"
    val F_NAMA = "nama"
    val F_LOKASI = "lokasi"
    var docId = ""
    lateinit var adapter: SimpleAdapter
    lateinit var db : FirebaseFirestore
    lateinit var alStudent :ArrayList<HashMap<String,Any>>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wisata)

        alStudent = arrayListOf()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsData.setOnItemClickListener(itemclick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener{ querySnapshot, e->
            if(e !=null) Log.d("firestore", e.message.toString())
            showData()
        }
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                val hm = HashMap<String,Any>()
                hm.put(F_ID,edId.text.toString())
                hm.put(F_NAMA,edName.text.toString())
                hm.put(F_LOKASI,edAdress.text.toString())
                            db.collection(COLLECTION).document(edId.text.toString()).set(hm).
                            addOnSuccessListener {
                                Toast.makeText(this,"Data Sukses Ditambahkan", Toast.LENGTH_SHORT)
                                    .show()
                            }.addOnFailureListener{ e ->
                                Toast.makeText(this,"Data gagal Ditambahkan :${e.message}", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
            R.id.btnUpdate ->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID, docId)
                hm.set(F_NAMA, edName.text.toString())
                hm.set(F_LOKASI, edAdress.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Data Sukses Diubah", Toast.LENGTH_SHORT)
                            .show()
                    }.addOnFailureListener{ e ->
                        Toast.makeText(this,"Data Gagal Diubah :${e.message}", Toast.LENGTH_SHORT)
                            .show()
                    }
            }
            R.id.btnDelete ->{
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        result ->
                    for (doc in result){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data Sukses Dihapus",
                                    Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e->
                                Toast.makeText(this, "Data Gagal Dihapus ${e.message}",
                                    Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Can't get data reference ${e.message}",
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    val itemclick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alStudent.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edName.setText(hm.get(F_NAMA).toString())
        edAdress.setText(hm.get(F_LOKASI).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_LOKASI,doc.get(F_LOKASI).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this,alStudent,R.layout.show_data_wisata,
                arrayOf(F_ID,F_NAMA,F_LOKASI),
                intArrayOf(R.id.txId,R.id.txName,R.id.txLocation))
            lsData.adapter=adapter
        }
    }
    }