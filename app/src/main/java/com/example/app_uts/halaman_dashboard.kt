package com.example.app_uts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

class halaman_dashboard:AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        showAbout.setOnClickListener(this)
        showWisata.setOnClickListener(this)
        showRoute.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.showRoute->{
                var aa = Intent(this,halaman_route::class.java)
                startActivity(aa)
            }

            R.id.showWisata->{
                var aa = Intent(this,halaman_wisata::class.java)
                startActivity(aa)
            }
            R.id.showAbout->{
                var aa = Intent(this,halaman_about::class.java)
                startActivity(aa)
            }
        }
    }
}