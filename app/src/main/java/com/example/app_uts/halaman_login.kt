package com.example.app_uts

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class halaman_login : AppCompatActivity(),View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSignUp -> {
                var email = edtxregEmail.text.toString()
                var password = edtxregPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username / password Harap Di Isi", Toast.LENGTH_LONG)
                        .show()
                } else {
                        val progressDialog = ProgressDialog(this)
                        progressDialog.isIndeterminate = true
                        progressDialog.setMessage("Tunggu Sebentar...")
                        progressDialog.show()

                        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener {
                                progressDialog.hide()
                                if (!it.isSuccessful) return@addOnCompleteListener
                                Toast.makeText(this, "Login Berhasil", Toast.LENGTH_SHORT).show()
                                val intent = Intent(this, halaman_dashboard::class.java)
                                startActivity(intent)
                            }
                            .addOnFailureListener {
                                progressDialog.hide()
                                Toast.makeText(
                                    this,
                                    "Username/password tidak valid", Toast.LENGTH_SHORT
                                ).show()
                            }
                }
            }
            R.id.txSignUp ->{
                var intent = Intent(this,halaman_signup::class.java)
                startActivity(intent)
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnSignUp.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }
}